import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  Vue,
  dsn: "https://4675ae9bf5394dbc9313e361bd660cbd@o487829.ingest.sentry.io/5547025",
  integrations: [
    new Integrations.BrowserTracing(),
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

new Vue({
  render: h => h(App),
}).$mount('#app')
